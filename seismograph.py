from obspy import read
from collections import defaultdict
from datetime import datetime
import numpy
import os
import sys
import json
import argparse
import paho.mqtt.client as mqtt


#Legge alcuni parametri da riga di comando
parser = argparse.ArgumentParser(description='Acquisce i dati necessari dall\'output di OpenSWPC e li invia al gateway tramite MQTT')
parser.add_argument('input', metavar = 'INPUT', help='path della cartella contenente i file .sac')
parser.add_argument('-b', '--broker', default='localhost', help='indirizzo IP o hostname del broker MQTT (default: 127.0.0.1/localhost)')
try:
    args = parser.parse_args()
except SystemExit:	#se il path non è specificato
    print("Errore: inserire il path dei file .sac!")
    sys.exit(1)
path = str(args.input)
if not any(fname.endswith('.sac') for fname in os.listdir(path)):
    print("Errore: il path specificato non contiene i file necessari!")
    sys.exit(1)
serverAddress = str(args.broker) 


#Connessione al broker MQTT
mqttClient = mqtt.Client()
mqttClient.connect(serverAddress)
mqttClient.loop_start()		#utile per riconnessione automatica


#Funzione che calcola il picco dell'onda sismica (onda s) e l'istante in cui si è verificato
def wave_peak(tr, t1, t2):
    maxi = tr.max()
    dt = t2 - t1
    num = dt / len(tr.data)
    tm = float(num * numpy.where(tr.data == maxi)[0])
    time = t1 + tm
    return time


#Funzione che prepara e invia i dati al broker
def send_to_MQTT(data):

    message = json.dumps(data)
    print(message)
    mqttClient.publish("topic/quake", message)


#Acquisizione dei dati necessari dall'output di OpenSWPC (file .sac)
groups = defaultdict(list)
quakes = defaultdict(list)

for filename in os.listdir(path):   #legge tutti i file dal path
    basename, extension = os.path.splitext(filename)
    swpc, n, station, velComponent = basename.split('.')
    groups[n+"."+station].append(path+filename)

for station in groups:	    #per ogni stazione
    time = []
    zero = True
    for filename in groups[station]:	    #per ogni file riguardante la stazione
        st = read(filename)
        tr = st[0]
        if numpy.count_nonzero(tr.data):   #se la stazione è interessata da qualche evento 
            name = tr.stats.station
            lat = float(tr.stats.sac.get('stla'))
            long = float(tr.stats.sac.get('stlo'))
            mag = float(tr.stats.sac.get('mag'))
            t1 = tr.stats.starttime
            tp = float(t1.timestamp)
            t2 = tr.stats.endtime
            t = wave_peak(tr, t1, t2)
            time.append(t)
            zero = False
    if not zero:
        ts = (float(time[0].timestamp) + float(time[1].timestamp) + float(time[2].timestamp)) / 3
        data = {
            'name_station' : name,
            'latitude' : lat,
            'longitude' : long,
            'magnitude' : mag,
            'time_s_wave' : ts,
            'time_p_wave' : tp
        }
        #Invio dei dati al broker MQTT
        send_to_MQTT(data) 
