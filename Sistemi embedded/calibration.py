#Importazione librerie
from sense_hat import SenseHat
from math import sqrt
from time import sleep
from datetime import datetime
from scipy import constants
from statistics import median


#Variabili globali
i = 0                   #Inizzializza contatore
value_x = []            #Lista dei valori rilevati per l'asse x
value_y = []            #Lista dei valori rilevati per l'asse y
value_z = []            #Lista dei valori rilevati per l'asse z


#Gestione del sensore 
sense = SenseHat()											    #Oggetto che rappresenta il sensore
sense._init_imu()											    #Inizializza il sensore
sense.set_imu_config(False, False, True)							                                    #Attiva solo l'accelerometro (disattiva giroscopio e magnetometro)


#Calibrazione dell'accelerometro per il sistema di riferimento
f = open('calibration.txt','w+',1)                                                          #Crea e apre un file txt sul quale scrive i valori trovati
f.write("Calibrazione dell'accelerometro per il sistema...\n")
print("Calibrazione dell'accelerometro per il sistema in corso...")

while i<100:                                                                                #Ciclo per la calibrazione (100 valori di riferimento)
    
    acceleration = sense.get_accelerometer_raw()                                            #Restituisce l'intensità dell'accerelazione in Gs
    x = acceleration['x']*constants.g
    y = acceleration['y']*constants.g
    z = acceleration['z']*constants.g
 
    f.write("x=%f, y=%f, z=%f\n" % (x, y, z))                                               #Salva i valori su file
    
    value_x.append(x)                                                                       #I valori rilevati vengono aggiunti nell'apposita lista
    value_y.append(y)
    value_z.append(z)
    
    time.sleep(1)
    
    i+=1

#Calcola la mediana dei valori per gli assi x, y e z e la salva su file
f.write("Media dei valori:\n")
f.write("x=%f, y=%f, z=%f\n" % (median(value_x), median(value_y), median(value_z))) 

f.close()                                                                                   #Chiude il file

print("Fatto!")
    
