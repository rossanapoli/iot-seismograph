//
//  ViewController.swift
//  Sismografo IoT
//
//  Created by Rossana Poli on 22/01/18.
//  Copyright © 2018 unibo.degree2018. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation
import CocoaMQTT

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    //Inizializzazione componenti necessari
    let motionManager = CMMotionManager()
    let locationManager = CLLocationManager()
    var manager: CMMotionActivityManager!
    
    
    //Variabili globali
    var stationary: Bool = false
    var start: Bool = false
    var calibrated: Int = 0
    var times: Int = 0
    var valueX = [Double]()
    var valueY = [Double]()
    var valueZ = [Double]()
    var x0: Double = 0.0
    var y0: Double = 0.0
    var z0: Double = 0.0
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    
    
    //Outlet elementi interfaccia
    @IBOutlet weak var valueMagLabel: UILabel!
    @IBOutlet weak var valueZLabel: UILabel!
    @IBOutlet weak var valueYLabel: UILabel!
    @IBOutlet weak var valueXLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var serverSwitch: UISwitch!
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var calibrationButton: UIButton!
    
    
    //Instanzia CocoaMQTT come mqttClient
    let mqttClient = CocoaMQTT(clientID: "iOS Device", host: "127.0.0.1", port: 1883)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Richiede permesso all'uso della localizzazione
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {    //Se il permesso è stato concesso, attiva la localizzazione
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        //Identifica l'attività dell'utente
        if CMMotionActivityManager.isActivityAvailable()
        {
            manager = CMMotionActivityManager()
            let queue = OperationQueue.main
            manager.startActivityUpdates(to: queue, withHandler: { (data) in
                if let values = data {
                    if values.stationary {  //Se il dispositivo è fermo
                        self.stationary = true
                    } else {
                        self.stationary = false
                    }
                }
                
            })
        }
        
        //Legge i dati dall'accelerometro
        motionManager.accelerometerUpdateInterval = 0.1
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data
            {
                self.updateAccelerometer(data: myData)
            }
        }
    }
    
    
    //Legge le coordinate del dispositivo
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
        }
    }
    
    
    //Se non viene concesso il permesso all'uso della geolocalizzazione, mostra un popup all'utente
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Accesso alla posizione disabilitato",
                                                message: "Occorre la posizione per inviare i dati al server",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Indietro", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Apri Impostazioni", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Mostra un messaggio di aiuto sull'utilizzo
    @IBAction func helpButton(_ sender: Any) {
        let alertController = UIAlertController(title: "Aiuto", message: "Il sismografo si attiva quando il telefono è appoggiato su una superficie orizzontale. Prima di avviarlo, clicca su \"Calibra\" per calibrare il sismografo.", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Indietro", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    //Avvia o interrompe la lettura dei dati
    @IBAction func startSeismograph(_ sender: Any) {
        if self.startStopButton.currentTitle == "Avvia"
        {
            self.startStopButton.setTitle("Stop", for: .normal)
            start = true
        } else {
            self.startStopButton.setTitle("Avvia", for: .normal)
            start = false
        }
    }
    

    //Effettua la "calibrazione" del dipositivo
    @IBAction func doCalibration(_ sender: Any) {
        let alertController = UIAlertController(title: "Sismografo calibrato!", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        self.calibrated = 1
        self.calibrationButton.isEnabled = false
        self.startStopButton.isEnabled = true
    }
    
    
    //Avvia o interrompe la connessione con il broker MQTT
    @IBAction func startServer(_ sender: UISwitch) {
        if sender.isOn == true {
            mqttClient.connect()
        } else {
            mqttClient.disconnect()
        }
    }
    
    
    //Invia i dati al broker MQTT componendo un messaggio in formato JSON
    func sendMQTT(message: Dictionary<String, Any>) {
        let jsonData = try! JSONSerialization.data(withJSONObject: message, options: [])
        let jsonString = (String(data: jsonData, encoding: String.Encoding.utf8))!
        mqttClient.publish("topic/quake", withString: jsonString)
    }
    
    
    //Funzione di supporto, che calcola la mediana da una lista di valori
    func calculateMedian(list: [Double]) -> Double {
        let sorted = list.sorted()
        if sorted.count % 2 == 0 {
            return (sorted[(sorted.count / 2)] + sorted[(sorted.count / 2) - 1]) / 2
        } else {
            return sorted[(sorted.count - 1) / 2]
        }
    }
    
    
    //Legge i dati effettivi dall'accelerometro e modifica le label relative
    func updateAccelerometer(data: CMAccelerometerData) {
        //Se non calibrato, effettua la "calibrazione"
        if self.calibrated == 1 {
            while self.times < 2000
            {
                self.valueX.append(data.acceleration.x * 9.81)
                self.valueY.append(data.acceleration.y * 9.81)
                self.valueZ.append(data.acceleration.z * 9.81)
                self.times += 1
            }
            self.x0 = self.calculateMedian(list: valueX)
            self.y0 = self.calculateMedian(list: valueY)
            self.z0 = self.calculateMedian(list: valueZ)
            self.calibrated = 2
        }
        //Se il dispositivo è calibrato e fermo, effettua le misurazioni
        if self.stationary == true && self.start == true && self.calibrated == 2 {
            let elevation = "\u{00B2}" //Simbolo per m/s^2
            let x = (data.acceleration.x * 9.81)-self.x0
            let y = (data.acceleration.y * 9.81)-self.y0
            let z = (data.acceleration.z * 9.81)-self.z0
            let mag = sqrt(x*x+y*y+z*z)
            
            self.valueXLabel.text = "\(String(format: "%.2f", x)) m\\s\(elevation)"
            self.valueYLabel.text = "\(String(format: "%.2f", y)) m\\s\(elevation)"
            self.valueZLabel.text = "\(String(format: "%.2f", z)) m\\s\(elevation)"
            self.valueMagLabel.text = "\(String(format: "%.2f", mag)) m\\s\(elevation)"
            //Quando viene rilevata una vibrazione, compone il messaggio da inviare
            if sqrt(x*x+y*y) > 0.1
            {
                let tp = NSDate().timeIntervalSince1970
                if mag > 1.0
                {
                    self.valueMagLabel.textColor = UIColor.red
                    self.messageLabel.text = "\(String(format: "%.2f", mag)) m\\s\(elevation)"
                    let ts = NSDate().timeIntervalSince1970
                    let name = UIDevice.current.name
                    let values = [
                        "name_station": name,
                        "latitude": latitude,
                        "longitude": longitude,
                        "magnitude": mag,
                        "time_s_wave": ts,
                        "time_p_wave": tp
                        ] as [String : Any]
                    self.sendMQTT(message: values)
                } else
                {
                    self.valueMagLabel.textColor = UIColor.black
                }
            }
        }
    }
}

