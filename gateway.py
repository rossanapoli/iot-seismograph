import json
import paho.mqtt.client as mqtt
import ibmiotf.device
import numpy
import _thread
import queue
import math
from datetime import datetime
from geopy.distance import vincenty
from statistics import median, mean


#Funzione che calcola la velocità dell'onda considerando la velocità media delle onde s e p
def wave_velocity(VS, VP):
    
    Vsp = (VS*VP) / (VP-VS)
    
    return Vsp 


#Funzione che calcola il tempo trascorso tra l'arrivo delle onde s e p
def delta_sec(p_time, s_time):
    
    delta = s_time - p_time
    
    return delta


#Funzione che calcola la distanza tra l'epicentro e la stazione
def calc_distance(delta_sec, Vsp):
    
    dist_to_eq = float(delta_sec * Vsp)
    
    return dist_to_eq


#Funzione che calcola l'epicentro del terremoto date tre stazioni
#Tratta da https://github.com/gsimore/earthquakes
def calc_epicenter(station1, station2, station3):
    
    earth_radius = 6371 #raggio della terra in kilometri
    
    #Calcola latitudine, longitudine, raggio delle tre stazioni in input
    lat1 = math.radians(station1['latitude'])     
    lon1 = math.radians(station1['longitude'])     
    r1 = calc_distance(delta_sec(station1['time_p_wave'], station1['time_s_wave']), Vsp)       
    lat2 = math.radians(station2['latitude'])     
    lon2 = math.radians(station2['longitude'])     
    r2 = calc_distance(delta_sec(station2['time_p_wave'], station2['time_s_wave']), Vsp)
    lat3 = math.radians(station3['latitude'])     
    lon3 = math.radians(station3['longitude'])     
    r3 = calc_distance(delta_sec(station3['time_p_wave'], station3['time_s_wave']), Vsp)
    
    #Converte latitudine e longitudine in coordinate xyz
    x1 = earth_radius * (math.cos(lat1) * math.cos(lon1))
    y1 = earth_radius * (math.cos(lat1) * math.sin(lon1))
    z1 = earth_radius * (math.sin(lat1))
    x2 = earth_radius * (math.cos(lat2) * math.cos(lon2))
    y2 = earth_radius * (math.cos(lat2) * math.sin(lon2))
    z2 = earth_radius * (math.sin(lat2))
    x3 = earth_radius * (math.cos(lat3) * math.cos(lon3))
    y3 = earth_radius * (math.cos(lat3) * math.sin(lon3))
    z3 = earth_radius * (math.sin(lat3))
    P1 = numpy.array([x1, y1, z1])  
    P2 = numpy.array([x2, y2, z2])
    P3 = numpy.array([x3, y3, z3])
    
    #Applica ciascun set di coordinate X, Y, Z per ciascuno dei 3 punti del proprio numpy array
    ex = (P2 - P1)/(numpy.linalg.norm(P2 - P1))                 
    i = numpy.dot(ex, P3 - P1)                                  
    ey = (P3 - P1 - i*ex)/(numpy.linalg.norm(P3 - P1 - i*ex))   
    ez = numpy.cross(ex, ey)                                    
    d = float(numpy.linalg.norm(P2 - P1))
    j = numpy.dot(ey, P3 - P1)
    
    #Calcola le coordinate x, y, z dell'epicentro
    x = ((r1**2) - (r2**2) + (d**2)) / (2*d)
    y = (((r1**2) - (r3**2) + (i**2) + (j**2))/(2*j)) - ((i/j)*x)
    z = math.sqrt(abs((r1**2) - (x**2) - (y**2)))
    tri_point = P1 + (x*ex) + (y*ey) + (z*ez)
    
    #Converte le coordinate x, y, z dell'epicentro in latitudine e longitudine
    lat = math.degrees(math.asin(tri_point[2] / earth_radius))
    lon = math.degrees(math.atan2(tri_point[1], tri_point[0]))
    epicenter = (lat, lon)
    
    return epicenter


#Funzione che restituisce alcune informazioni sul terremoto come magnitudo ed epicentro
def calc_earthquake_info(quakes):
    
    num = len(quakes)   #numero delle stazioni
    epic_lat = []       #lista per la latitudine
    epic_lon = []       #lista per la longitudine
    
    #Calcola la magnitudo del terremoto
    magnitude = median([x['magnitude'] for x in quakes])
    
    #Calcola l'epicentro del terremoto
    for i in range(0, num-2):
        try:
            epic = calc_epicenter(quakes[i], quakes[i+1], quakes[i+2]) #considero le stazioni a gruppi di tre
            epic_lat.append(epic[0]) 
            epic_lon.append(epic[1]) 
        except ValueError as e:
            pass

    epicenter = (mean(epic_lat), mean(epic_lon))    #per minimizzare l'errore sul calcolo
    
    return (magnitude, epicenter)


#Funzione che calcola il valore ottimale di epsilon per l'insieme dei dati considerati
def calc_epsilon(sensors):

    num = len(sensors)        		
    i = 0                           
    distances = []
    
    while i < num - 2:
        #Prende i dati della i-esima stazione
        stationA = sensors[i]
        coordA = (stationA['latitude'], stationA['longitude']) 
        timeA = stationA['time_s_wave']
        #Prende i dati da tutte le altre stazioni
        for j in range(i+1, num):
            stationB = sensors[j]
            coordB = (stationB['latitude'], stationB['longitude'])
            timeB = stationB['time_s_wave']
            d = vincenty(coordA, coordB).kilometers
            value = abs(d - (Vsp * (timeB - timeA)))
            distances.append(value)
        i += 1

    return numpy.percentile(distances, 15) 


#Funzione che verifica se l'evento rivelato è effetivamente un terremoto
def verify_earthquake(sensors):
    
    num = len(sensors)        		
    cont = 0                        #contatore di confronti con esito positivo
    i = 0  

	epsilon = calc_epsilon(sensors)
    
    #Verifica se i dati sono correlati ad un evento sismico oppure no
    while i < num - 2:
        #Prende i dati della i-esima stazione
        stationA = sensors[i]
        coordA = (stationA['latitude'], stationA['longitude']) 
        timeA = stationA['time_s_wave']
        #Prende i dati da tutte le altre stazioni
        for j in range(i+1, num):
            stationB = sensors[j]
            coordB = (stationB['latitude'], stationB['longitude'])
            timeB = stationB['time_s_wave']
            #Calcola la distanza tra le stazioni
            d = vincenty(coordA, coordB).kilometers
            #Verifica la formula |d-v*t|
            if abs(d - (Vsp * (timeB - timeA))) < epsilon:	
                cont += 1
        i += 1
        
    #Verifica se la vibrazione è stata percepita da almeno il 10% dei sismografi
    if cont / num > 0.1:
        print("Si è verificato un terremoto!")
        return True
    else:
        return False


#Funzione che invia i dati alla piattaforma IBM Watson IoT
def send_to_IBM(magnitude, epicenter):
    
    #Struttura il messaggio da inviare
    data = {
        'd' : {
            'magnitude' : magnitude,
            'location' : {
                'longitude': epicenter[1],
                'latitude': epicenter[0] 
            }
        }
    }
    
    #Pubblica il messaggio sulla piattaforma
    deviceCli.publishEvent("earthquake", "json", data)


#Funzione che cancella tutti i dati più vecchi di n secondi
def clear_old_data(list_data, n):
    
    timestamp = float(datetime.now().timestamp())
    new_data = []
    
    for x in list_data:
        delta = abs(timestamp - x['timestamp'])
        if delta < n:
            new_data.append(x)
            
    return new_data


#Funzione che definisce un consumatore per il thread (legge i dati e li analizza)
def consumer_thread(q):
    
    list_value = []
    
    while True:
        try:
            value = q.get(True, 10)     
            list_value.append(value)
            if len(list_value) > 500:   #limite massimo di messaggi
                raise queue.Empty()
        except queue.Empty as e:	    #non ci sono nuovi messaggi (considera i precedenti come riferiti ad un unico evento)
            data = clear_old_data(list_value, 60)
            list_value = []
            if len(data) >= 3:		#occorrono almeno i dati di tre stazioni per analizzare un evento
                if verify_earthquake(data):
                    quake = calc_earthquake_info(data)
                    send_to_IBM(quake[0], quake[1])
                    print("Magnitudo: %.2f" %(quake[0]))
                    print("Epicentro: (%.4f, %.4f)" %(quake[1][0], quake[1][1]))
			
			
#Funzione richiamata quando il client si connette al broker MQTT
def on_connect(client, userdata, flags, rc):
    mqttClient.subscribe("topic/quake")


#Funzione richiamata quando il client riceve un messaggio
def on_message(client, myqueue, msg):
    
    message = msg.payload.decode(encoding='UTF-8')
    data = json.loads(message)
    timestamp = float(datetime.now().timestamp())
    data['timestamp'] = timestamp
    myqueue.put(data)


#Connessione alla piattaforma IBM Watson IoT
deviceOptions = ibmiotf.device.ParseConfigFile('device.cfg')	#richiede il file che contiene le opzioni del dispositivo
deviceCli = ibmiotf.device.Client(deviceOptions)
deviceCli.connect()


#Connessione al broker MQTT
serverAddress = "127.0.0.1"
mqttClient = mqtt.Client()
mqttClient.connect(serverAddress)
mqttClient.on_connect = on_connect
mqttClient.on_message = on_message


#Crea un thread consumer che analizza i messaaggi arrivati
myqueue = queue.Queue()     #coda per i thread
mqttClient.user_data_set(myqueue)
_thread.start_new_thread(consumer_thread, (myqueue,))


#Calcola la velocità delle onde sismiche nel territorio di riferimento
Vsp = wave_velocity(VS=3.47, VP=5.90)		#default: velocità onde s e p nelle rocce crostali del Giappone


#Rimane in attesa di messaggi
mqttClient.loop_forever()
