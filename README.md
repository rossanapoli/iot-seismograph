# README #

Questa repository contiene i file relativi al progetto di tesi con titolo "Edge computing e Internet of Things per un sistema di allerta terremoti". In particolare è stato realizzato un sistema IoT per la rivelazione e la notifica di eventi sismici.

### A cosa serve questa repository ###

La repository contiene il necessario per far funzionare il sistema, inclusi codice Python e il progetto XCode dell'applicazione realizzata. 
 
### Il funzionamento del sistema e il contenuto della repository ###

Il sistema prevede l'utilizzo di diversi sensori come sistemi embedded e smartphone dotati di accelerometri, che registrino i dati e li inviino al gateway. 
Per supplire alla mancanza di numerosi sensori è possibile utilizzare, a scopo di test del sistema, una simulazione software che simuli un terremoto e la sua individuazione da parte di migliaia di sensori sparsi su un determinato territorio. A questo scopo è stato utilizzato il software OpenSWPC, un simulatore di propagazione delle onde sismiche. Il codice relativo al software, con il file di input utilizzato, il conseguente output generato e una guida per l'utilizzo è disponibile al link https://www.dropbox.com/sh/n4ljsa83ed67vwc/AADhZ_OLuJiYsQ3Eeyw9pEUwa?dl=0. Il file "seismograph.py" contiene il codice necessario per leggere i file di output prodotti da OpenSWPC ed estrarre i dati relativi ad ogni stazione come magnitudo, latitudine, longitudine, il picco di velocità dell'onda e l'istante di tempo in cui si è verificato. Tali dati vengono inviati tramite MQTT al gateway.
La cartella "sistemi_embedded" contiene il codice per utilizzare come sensori Raspberry Pi 3 con l'accelerometro incluso nella board di espansione Sense Hat e gli smartphone iOS. Tali applicazioni sono state realizzate per dare un'idea sull'effettivo utilizzo di questi dispositivi nel sistema (è pertanto soggetto a migliorie ed a ulteriori test).

Il gateway IoT, realizzato con Raspberry Pi 3, riceve i dati provenienti dai sensori, verifica la veridicità dell'evento sismico e le sue caratteristiche, e li pubblica sulla piattaforma IBM Watson IoT. Il gateway deve installare un broker MQTT; a tale scopo si può utilizzare Mosquitto. Il file "gateway.py" contiene il codice necessario per ricevere i dati delle stazioni tramite MQTT e applica una formula di fisica per determinare se si è verificato effettivamente un evento sismico. In caso positivo, invia i dati alla piattaforma IoT. 

La piattaforma IoT, in questo caso IBM Watson IoT, si occupa della notifica degli eventi sismici, inviando una mail con i dati relativi a particolari soggetti e pubblicando un tweet su Twitter. 

### Requisiti hardware/software ###

Per utilizzare il progetto occorre:
1. Raspberry Pi 3 Model B (https://www.raspberrypi.org/products/raspberry-pi-3-model-b/)
2. Installare Python 3 o successivi (https://www.python.org/download/releases/3.0/)
3. Installare le librerie per Python ObsPy, GeoPy, Paho MQTT e Python per IBM Watson IoT (https://github.com/ibm-watson-iot/iot-python)
4. Installare il broker MQTT Mosquitto (https://mosquitto.org)
5. Registrarsi sulla piattaforma IBM Bluemix
6. Registrarsi al servizio IFTTT
7. Possedere un'account Twitter (facoltativo)

### Link esterni ###
Software OpenSWPC:
https://www.dropbox.com/sh/n4ljsa83ed67vwc/AADhZ_OLuJiYsQ3Eeyw9pEUwa?dl=0

### Contributi ###

Il progetto di tesi, per la laurea magistrale in Informatica, è stato realizzato da Rossana Poli. Si ringrazia, per l'aiuto e i consigli circa l'ideazione o lo sviluppo del progetto, il personale del CNAF INFN di Bologna. 

Per il software OpenSWPC originale, disponibile al link https://github.com/takuto-maeda/OpenSWPC, si ringrazia l'autore Takuto Maeda.  

### Contatti ###

Per informazioni, segnalazioni e consigli per ulteriore sviluppo, scrivere a rossana.poli@studio.unibo.it
